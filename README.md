# README

Find script for cleaning, parsing, loading, and writing valid_carefirst_bluechoice_doctors.csv in:

* app/models/data_cleaner.rb
* app/models/load_csv.rb


Find a doctor function can be found in:

* app/models/physician.rb

This function could be called from a front-end by sending a post request to physicians/new?action=find_a_doctor. Requests to the /new route call functions provided in the request parameters and feed in data to those functions from the body of the post request

In javascript this could look like:

```
fetch('http://localhost:3000/physicians/new?action=find_a_doctor',{
      method: 'POST',
      redirect: 'follow', // manual, *follow, error

      headers: {'Content-Type': 'application/json'},
      mode: 'cors',
      body: JSON.stringify({location: 22202, specialty: pediatrician}),
    }).then(resp => resp.json()).then(data => results = data); 
```

To be added: 

* function to determine most dense population of doctors in a given radius. Likely should be written in something like python using the pandas library, but from a high level in Ruby I would

    * Write the function in the physicians file in a similar fashion to the find_a_doctor
    * Use search criteria to run a moving window type search, possibly with the rGeo gem
    * Given lack of great support for this function in rails, alternatively brute force by
        * Create empty array called something like most_dense
        * Iterate over all Physicians with valid_npi
            * find any doctors within r miles of latitude
                * check if also within r miles of longitude
            * replace doctors in most_dense array if number with r miles is greater than current length of most_dense array
        * pluck and average longitudes and latitudes from all offices in most_dense array
