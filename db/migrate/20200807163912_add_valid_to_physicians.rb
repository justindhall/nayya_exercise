class AddValidToPhysicians < ActiveRecord::Migration[6.0]
  def change
    add_column :physicians, :valid_npi, :boolean, default: false
  end
end
