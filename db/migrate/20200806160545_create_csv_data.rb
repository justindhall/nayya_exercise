class CreateCsvData < ActiveRecord::Migration[6.0]
  def change
    create_table :csv_data do |t|
      t.jsonb :entry

      t.timestamps
    end
  end
end
