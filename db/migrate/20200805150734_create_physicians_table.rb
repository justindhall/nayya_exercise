class CreatePhysiciansTable < ActiveRecord::Migration[6.0]
  def change
    create_table :physicians, {:id => false} do |t|
      t.string :email
      t.string :gender
      t.string :first_name
      t.bigint :id
      t.string :last_name
      t.string :middle_name
      t.string :network_ids, array: true
      t.string :organization_name
      t.string :phone
      t.string :presentation_name
      t.string :specialty
      t.string :suffix
      t.string :title
      t.string :data_type
      t.string :npis, array: true
      t.jsonb :addresses, default: '{}'

      t.timestamps
    end
  end
end
