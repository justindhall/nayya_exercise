# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_08_07_163912) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "csv_data", force: :cascade do |t|
    t.jsonb "entry"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "physicians", id: false, force: :cascade do |t|
    t.string "email"
    t.string "gender"
    t.string "first_name"
    t.bigint "id"
    t.string "last_name"
    t.string "middle_name"
    t.string "network_ids", array: true
    t.string "organization_name"
    t.string "phone"
    t.string "presentation_name"
    t.string "specialty"
    t.string "suffix"
    t.string "title"
    t.string "data_type"
    t.string "npis", array: true
    t.jsonb "addresses", default: "{}"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "valid_npi", default: false
  end

  create_table "revoked_licenses", id: false, force: :cascade do |t|
    t.string "npi_0", limit: 255
    t.index ["npi_0"], name: "revoked_licenses_npi_0_idx"
  end

  create_table "temp_1596812374", id: false, force: :cascade do |t|
    t.string "npi_0", limit: 255
    t.string "entity_type_code_1", limit: 255
    t.string "replacement_npi_2", limit: 255
    t.string "employer_identification_number__ein__3", limit: 255
    t.string "provider_organization_name__legal_business_name__4", limit: 255
    t.string "provider_last_name__legal_name__5", limit: 255
    t.string "provider_first_name_6", limit: 255
    t.string "provider_middle_name_7", limit: 255
    t.string "provider_name_prefix_text_8", limit: 255
    t.string "provider_name_suffix_text_9", limit: 255
    t.string "provider_credential_text_10", limit: 255
    t.string "provider_other_organization_name_11", limit: 255
    t.string "provider_other_organization_name_type_code_12", limit: 255
    t.string "provider_other_last_name_13", limit: 255
    t.string "provider_other_first_name_14", limit: 255
    t.string "provider_other_middle_name_15", limit: 255
    t.string "provider_other_name_prefix_text_16", limit: 255
    t.string "provider_other_name_suffix_text_17", limit: 255
    t.string "provider_other_credential_text_18", limit: 255
    t.string "provider_other_last_name_type_code_19", limit: 255
    t.string "provider_first_line_business_mailing_address_20", limit: 255
    t.string "provider_second_line_business_mailing_address_21", limit: 255
    t.string "provider_business_mailing_address_city_name_22", limit: 255
    t.string "provider_business_mailing_address_state_name_23", limit: 255
    t.string "provider_business_mailing_address_postal_code_24", limit: 255
    t.string "provider_business_mailing_address_country_code__if_outside_u_s_", limit: 255
    t.string "provider_business_mailing_address_telephone_number_26", limit: 255
    t.string "provider_business_mailing_address_fax_number_27", limit: 255
    t.string "provider_first_line_business_practice_location_address_28", limit: 255
    t.string "provider_second_line_business_practice_location_address_29", limit: 255
    t.string "provider_business_practice_location_address_city_name_30", limit: 255
    t.string "provider_business_practice_location_address_state_name_31", limit: 255
    t.string "provider_business_practice_location_address_postal_code_32", limit: 255
    t.string "provider_business_practice_location_address_country_code__if_ou", limit: 255
    t.string "provider_business_practice_location_address_telephone_number_34", limit: 255
    t.string "provider_business_practice_location_address_fax_number_35", limit: 255
    t.string "provider_enumeration_date_36", limit: 255
    t.string "last_update_date_37", limit: 255
    t.string "npi_deactivation_reason_code_38", limit: 255
    t.string "npi_deactivation_date_39", limit: 255
    t.string "npi_reactivation_date_40", limit: 255
    t.string "provider_gender_code_41", limit: 255
    t.string "authorized_official_last_name_42", limit: 255
    t.string "authorized_official_first_name_43", limit: 255
    t.string "authorized_official_middle_name_44", limit: 255
    t.string "authorized_official_title_or_position_45", limit: 255
    t.string "authorized_official_telephone_number_46", limit: 255
    t.string "healthcare_provider_taxonomy_code___47", limit: 255
    t.string "provider_license_number___48", limit: 255
    t.string "provider_license_number_state_code___49", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch___50", limit: 255
    t.string "healthcare_provider_taxonomy_code___51", limit: 255
    t.string "provider_license_number___52", limit: 255
    t.string "provider_license_number_state_code___53", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch___54", limit: 255
    t.string "healthcare_provider_taxonomy_code___55", limit: 255
    t.string "provider_license_number___56", limit: 255
    t.string "provider_license_number_state_code___57", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch___58", limit: 255
    t.string "healthcare_provider_taxonomy_code___59", limit: 255
    t.string "provider_license_number___60", limit: 255
    t.string "provider_license_number_state_code___61", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch___62", limit: 255
    t.string "healthcare_provider_taxonomy_code___63", limit: 255
    t.string "provider_license_number___64", limit: 255
    t.string "provider_license_number_state_code___65", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch___66", limit: 255
    t.string "healthcare_provider_taxonomy_code___67", limit: 255
    t.string "provider_license_number___68", limit: 255
    t.string "provider_license_number_state_code___69", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch___70", limit: 255
    t.string "healthcare_provider_taxonomy_code___71", limit: 255
    t.string "provider_license_number___72", limit: 255
    t.string "provider_license_number_state_code___73", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch___74", limit: 255
    t.string "healthcare_provider_taxonomy_code___75", limit: 255
    t.string "provider_license_number___76", limit: 255
    t.string "provider_license_number_state_code___77", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch___78", limit: 255
    t.string "healthcare_provider_taxonomy_code___79", limit: 255
    t.string "provider_license_number___80", limit: 255
    t.string "provider_license_number_state_code___81", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch___82", limit: 255
    t.string "healthcare_provider_taxonomy_code____83", limit: 255
    t.string "provider_license_number____84", limit: 255
    t.string "provider_license_number_state_code____85", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch____86", limit: 255
    t.string "healthcare_provider_taxonomy_code____87", limit: 255
    t.string "provider_license_number____88", limit: 255
    t.string "provider_license_number_state_code____89", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch____90", limit: 255
    t.string "healthcare_provider_taxonomy_code____91", limit: 255
    t.string "provider_license_number____92", limit: 255
    t.string "provider_license_number_state_code____93", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch____94", limit: 255
    t.string "healthcare_provider_taxonomy_code____95", limit: 255
    t.string "provider_license_number____96", limit: 255
    t.string "provider_license_number_state_code____97", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch____98", limit: 255
    t.string "healthcare_provider_taxonomy_code____99", limit: 255
    t.string "provider_license_number____100", limit: 255
    t.string "provider_license_number_state_code____101", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch____102", limit: 255
    t.string "healthcare_provider_taxonomy_code____103", limit: 255
    t.string "provider_license_number____104", limit: 255
    t.string "provider_license_number_state_code____105", limit: 255
    t.string "healthcare_provider_primary_taxonomy_switch____106", limit: 255
    t.string "other_provider_identifier___107", limit: 255
    t.string "other_provider_identifier_type_code___108", limit: 255
    t.string "other_provider_identifier_state___109", limit: 255
    t.string "other_provider_identifier_issuer___110", limit: 255
    t.string "other_provider_identifier___111", limit: 255
    t.string "other_provider_identifier_type_code___112", limit: 255
    t.string "other_provider_identifier_state___113", limit: 255
    t.string "other_provider_identifier_issuer___114", limit: 255
    t.string "other_provider_identifier___115", limit: 255
    t.string "other_provider_identifier_type_code___116", limit: 255
    t.string "other_provider_identifier_state___117", limit: 255
    t.string "other_provider_identifier_issuer___118", limit: 255
    t.string "other_provider_identifier___119", limit: 255
    t.string "other_provider_identifier_type_code___120", limit: 255
    t.string "other_provider_identifier_state___121", limit: 255
    t.string "other_provider_identifier_issuer___122", limit: 255
    t.string "other_provider_identifier___123", limit: 255
    t.string "other_provider_identifier_type_code___124", limit: 255
    t.string "other_provider_identifier_state___125", limit: 255
    t.string "other_provider_identifier_issuer___126", limit: 255
    t.string "other_provider_identifier___127", limit: 255
    t.string "other_provider_identifier_type_code___128", limit: 255
    t.string "other_provider_identifier_state___129", limit: 255
    t.string "other_provider_identifier_issuer___130", limit: 255
    t.string "other_provider_identifier___131", limit: 255
    t.string "other_provider_identifier_type_code___132", limit: 255
    t.string "other_provider_identifier_state___133", limit: 255
    t.string "other_provider_identifier_issuer___134", limit: 255
    t.string "other_provider_identifier___135", limit: 255
    t.string "other_provider_identifier_type_code___136", limit: 255
    t.string "other_provider_identifier_state___137", limit: 255
    t.string "other_provider_identifier_issuer___138", limit: 255
    t.string "other_provider_identifier___139", limit: 255
    t.string "other_provider_identifier_type_code___140", limit: 255
    t.string "other_provider_identifier_state___141", limit: 255
    t.string "other_provider_identifier_issuer___142", limit: 255
    t.string "other_provider_identifier____143", limit: 255
    t.string "other_provider_identifier_type_code____144", limit: 255
    t.string "other_provider_identifier_state____145", limit: 255
    t.string "other_provider_identifier_issuer____146", limit: 255
    t.string "other_provider_identifier____147", limit: 255
    t.string "other_provider_identifier_type_code____148", limit: 255
    t.string "other_provider_identifier_state____149", limit: 255
    t.string "other_provider_identifier_issuer____150", limit: 255
    t.string "other_provider_identifier____151", limit: 255
    t.string "other_provider_identifier_type_code____152", limit: 255
    t.string "other_provider_identifier_state____153", limit: 255
    t.string "other_provider_identifier_issuer____154", limit: 255
    t.string "other_provider_identifier____155", limit: 255
    t.string "other_provider_identifier_type_code____156", limit: 255
    t.string "other_provider_identifier_state____157", limit: 255
    t.string "other_provider_identifier_issuer____158", limit: 255
    t.string "other_provider_identifier____159", limit: 255
    t.string "other_provider_identifier_type_code____160", limit: 255
    t.string "other_provider_identifier_state____161", limit: 255
    t.string "other_provider_identifier_issuer____162", limit: 255
    t.string "other_provider_identifier____163", limit: 255
    t.string "other_provider_identifier_type_code____164", limit: 255
    t.string "other_provider_identifier_state____165", limit: 255
    t.string "other_provider_identifier_issuer____166", limit: 255
    t.string "other_provider_identifier____167", limit: 255
    t.string "other_provider_identifier_type_code____168", limit: 255
    t.string "other_provider_identifier_state____169", limit: 255
    t.string "other_provider_identifier_issuer____170", limit: 255
    t.string "other_provider_identifier____171", limit: 255
    t.string "other_provider_identifier_type_code____172", limit: 255
    t.string "other_provider_identifier_state____173", limit: 255
    t.string "other_provider_identifier_issuer____174", limit: 255
    t.string "other_provider_identifier____175", limit: 255
    t.string "other_provider_identifier_type_code____176", limit: 255
    t.string "other_provider_identifier_state____177", limit: 255
    t.string "other_provider_identifier_issuer____178", limit: 255
    t.string "other_provider_identifier____179", limit: 255
    t.string "other_provider_identifier_type_code____180", limit: 255
    t.string "other_provider_identifier_state____181", limit: 255
    t.string "other_provider_identifier_issuer____182", limit: 255
    t.string "other_provider_identifier____183", limit: 255
    t.string "other_provider_identifier_type_code____184", limit: 255
    t.string "other_provider_identifier_state____185", limit: 255
    t.string "other_provider_identifier_issuer____186", limit: 255
    t.string "other_provider_identifier____187", limit: 255
    t.string "other_provider_identifier_type_code____188", limit: 255
    t.string "other_provider_identifier_state____189", limit: 255
    t.string "other_provider_identifier_issuer____190", limit: 255
    t.string "other_provider_identifier____191", limit: 255
    t.string "other_provider_identifier_type_code____192", limit: 255
    t.string "other_provider_identifier_state____193", limit: 255
    t.string "other_provider_identifier_issuer____194", limit: 255
    t.string "other_provider_identifier____195", limit: 255
    t.string "other_provider_identifier_type_code____196", limit: 255
    t.string "other_provider_identifier_state____197", limit: 255
    t.string "other_provider_identifier_issuer____198", limit: 255
    t.string "other_provider_identifier____199", limit: 255
    t.string "other_provider_identifier_type_code____200", limit: 255
    t.string "other_provider_identifier_state____201", limit: 255
    t.string "other_provider_identifier_issuer____202", limit: 255
    t.string "other_provider_identifier____203", limit: 255
    t.string "other_provider_identifier_type_code____204", limit: 255
    t.string "other_provider_identifier_state____205", limit: 255
    t.string "other_provider_identifier_issuer____206", limit: 255
    t.string "other_provider_identifier____207", limit: 255
    t.string "other_provider_identifier_type_code____208", limit: 255
    t.string "other_provider_identifier_state____209", limit: 255
    t.string "other_provider_identifier_issuer____210", limit: 255
    t.string "other_provider_identifier____211", limit: 255
    t.string "other_provider_identifier_type_code____212", limit: 255
    t.string "other_provider_identifier_state____213", limit: 255
    t.string "other_provider_identifier_issuer____214", limit: 255
    t.string "other_provider_identifier____215", limit: 255
    t.string "other_provider_identifier_type_code____216", limit: 255
    t.string "other_provider_identifier_state____217", limit: 255
    t.string "other_provider_identifier_issuer____218", limit: 255
    t.string "other_provider_identifier____219", limit: 255
    t.string "other_provider_identifier_type_code____220", limit: 255
    t.string "other_provider_identifier_state____221", limit: 255
    t.string "other_provider_identifier_issuer____222", limit: 255
    t.string "other_provider_identifier____223", limit: 255
    t.string "other_provider_identifier_type_code____224", limit: 255
    t.string "other_provider_identifier_state____225", limit: 255
    t.string "other_provider_identifier_issuer____226", limit: 255
    t.string "other_provider_identifier____227", limit: 255
    t.string "other_provider_identifier_type_code____228", limit: 255
    t.string "other_provider_identifier_state____229", limit: 255
    t.string "other_provider_identifier_issuer____230", limit: 255
    t.string "other_provider_identifier____231", limit: 255
    t.string "other_provider_identifier_type_code____232", limit: 255
    t.string "other_provider_identifier_state____233", limit: 255
    t.string "other_provider_identifier_issuer____234", limit: 255
    t.string "other_provider_identifier____235", limit: 255
    t.string "other_provider_identifier_type_code____236", limit: 255
    t.string "other_provider_identifier_state____237", limit: 255
    t.string "other_provider_identifier_issuer____238", limit: 255
    t.string "other_provider_identifier____239", limit: 255
    t.string "other_provider_identifier_type_code____240", limit: 255
    t.string "other_provider_identifier_state____241", limit: 255
    t.string "other_provider_identifier_issuer____242", limit: 255
    t.string "other_provider_identifier____243", limit: 255
    t.string "other_provider_identifier_type_code____244", limit: 255
    t.string "other_provider_identifier_state____245", limit: 255
    t.string "other_provider_identifier_issuer____246", limit: 255
    t.string "other_provider_identifier____247", limit: 255
    t.string "other_provider_identifier_type_code____248", limit: 255
    t.string "other_provider_identifier_state____249", limit: 255
    t.string "other_provider_identifier_issuer____250", limit: 255
    t.string "other_provider_identifier____251", limit: 255
    t.string "other_provider_identifier_type_code____252", limit: 255
    t.string "other_provider_identifier_state____253", limit: 255
    t.string "other_provider_identifier_issuer____254", limit: 255
    t.string "other_provider_identifier____255", limit: 255
    t.string "other_provider_identifier_type_code____256", limit: 255
    t.string "other_provider_identifier_state____257", limit: 255
    t.string "other_provider_identifier_issuer____258", limit: 255
    t.string "other_provider_identifier____259", limit: 255
    t.string "other_provider_identifier_type_code____260", limit: 255
    t.string "other_provider_identifier_state____261", limit: 255
    t.string "other_provider_identifier_issuer____262", limit: 255
    t.string "other_provider_identifier____263", limit: 255
    t.string "other_provider_identifier_type_code____264", limit: 255
    t.string "other_provider_identifier_state____265", limit: 255
    t.string "other_provider_identifier_issuer____266", limit: 255
    t.string "other_provider_identifier____267", limit: 255
    t.string "other_provider_identifier_type_code____268", limit: 255
    t.string "other_provider_identifier_state____269", limit: 255
    t.string "other_provider_identifier_issuer____270", limit: 255
    t.string "other_provider_identifier____271", limit: 255
    t.string "other_provider_identifier_type_code____272", limit: 255
    t.string "other_provider_identifier_state____273", limit: 255
    t.string "other_provider_identifier_issuer____274", limit: 255
    t.string "other_provider_identifier____275", limit: 255
    t.string "other_provider_identifier_type_code____276", limit: 255
    t.string "other_provider_identifier_state____277", limit: 255
    t.string "other_provider_identifier_issuer____278", limit: 255
    t.string "other_provider_identifier____279", limit: 255
    t.string "other_provider_identifier_type_code____280", limit: 255
    t.string "other_provider_identifier_state____281", limit: 255
    t.string "other_provider_identifier_issuer____282", limit: 255
    t.string "other_provider_identifier____283", limit: 255
    t.string "other_provider_identifier_type_code____284", limit: 255
    t.string "other_provider_identifier_state____285", limit: 255
    t.string "other_provider_identifier_issuer____286", limit: 255
    t.string "other_provider_identifier____287", limit: 255
    t.string "other_provider_identifier_type_code____288", limit: 255
    t.string "other_provider_identifier_state____289", limit: 255
    t.string "other_provider_identifier_issuer____290", limit: 255
    t.string "other_provider_identifier____291", limit: 255
    t.string "other_provider_identifier_type_code____292", limit: 255
    t.string "other_provider_identifier_state____293", limit: 255
    t.string "other_provider_identifier_issuer____294", limit: 255
    t.string "other_provider_identifier____295", limit: 255
    t.string "other_provider_identifier_type_code____296", limit: 255
    t.string "other_provider_identifier_state____297", limit: 255
    t.string "other_provider_identifier_issuer____298", limit: 255
    t.string "other_provider_identifier____299", limit: 255
    t.string "other_provider_identifier_type_code____300", limit: 255
    t.string "other_provider_identifier_state____301", limit: 255
    t.string "other_provider_identifier_issuer____302", limit: 255
    t.string "other_provider_identifier____303", limit: 255
    t.string "other_provider_identifier_type_code____304", limit: 255
    t.string "other_provider_identifier_state____305", limit: 255
    t.string "other_provider_identifier_issuer____306", limit: 255
    t.string "is_sole_proprietor_307", limit: 255
    t.string "is_organization_subpart_308", limit: 255
    t.string "parent_organization_lbn_309", limit: 255
    t.string "parent_organization_tin_310", limit: 255
    t.string "authorized_official_name_prefix_text_311", limit: 255
    t.string "authorized_official_name_suffix_text_312", limit: 255
    t.string "authorized_official_credential_text_313", limit: 255
    t.string "healthcare_provider_taxonomy_group___314", limit: 255
    t.string "healthcare_provider_taxonomy_group___315", limit: 255
    t.string "healthcare_provider_taxonomy_group___316", limit: 255
    t.string "healthcare_provider_taxonomy_group___317", limit: 255
    t.string "healthcare_provider_taxonomy_group___318", limit: 255
    t.string "healthcare_provider_taxonomy_group___319", limit: 255
    t.string "healthcare_provider_taxonomy_group___320", limit: 255
    t.string "healthcare_provider_taxonomy_group___321", limit: 255
    t.string "healthcare_provider_taxonomy_group___322", limit: 255
    t.string "healthcare_provider_taxonomy_group____323", limit: 255
    t.string "healthcare_provider_taxonomy_group____324", limit: 255
    t.string "healthcare_provider_taxonomy_group____325", limit: 255
    t.string "healthcare_provider_taxonomy_group____326", limit: 255
    t.string "healthcare_provider_taxonomy_group____327", limit: 255
    t.string "healthcare_provider_taxonomy_group____328", limit: 255
    t.string "certification_date__329", limit: 255
    t.index ["npi_deactivation_date_39"], name: "temp_1596812374_npi_deactivation_date_39_idx"
    t.index ["npi_reactivation_date_40"], name: "temp_1596812374_npi_reactivation_date_40_idx"
  end

end
