module Rest
  module Exception
    class AccessDenied < StandardError
      attr_reader :action
      attr_reader :controller
      def initialize(controller, action)
        super("No user for session on #{action} #{controller}")
        @action = action
        @controller = controller
      end
    end
  end
end
