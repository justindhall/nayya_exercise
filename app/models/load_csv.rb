class LoadCsv

  def database_config
    @database_config ||= {
        password: ActiveRecord::Base.connection_config[:password],
        host: ActiveRecord::Base.connection_config[:host],
        username: ActiveRecord::Base.connection_config[:username],
        database: ActiveRecord::Base.connection_config[:database]
    }
  end

  def psql(command)
    psql_command = "PGPASSWORD=#{database_config[:password]} psql -h #{database_config[:host]} -U #{database_config[:username]} -d #{database_config[:database]} -c \"#{command}\""
    result = system(psql_command)
    if result == true
      result
    else
      puts psql_command
      raise StandardError "Failed to create #{temp_table_name}"
      false
    end
  end

    def temp_table_name
      @temp_table_name ||= "temp_#{DateTime.now.to_i}"
    end

    def load_input_file
      "#{Rails.root}/tmp/data/npi_master_list.csv"
    end

    def copy
      psql("CREATE TABLE #{temp_table_name} (#{temp_table_columns_with_types});")
      psql("CREATE INDEX ON #{temp_table_name} (npi_deactivation_date_39)")
      psql("CREATE INDEX ON #{temp_table_name} (npi_reactivation_date_40)")
      psql("\\copy #{temp_table_name} FROM '#{load_input_file}' DELIMITER ',' CSV")
      psql("CREATE TABLE revoked_licenses AS SELECT npi_0 FROM #{temp_table_name} WHERE npi_deactivation_date_39 <> '' AND npi_reactivation_date_40 = ''")
      psql("CREATE INDEX ON revoked_licenses (npi_0)")
      psql("UPDATE physicians SET valid_npi = true WHERE id IN (SELECT t1.id FROM physicians t1 WHERE NOT EXISTS (SELECT t2.NPI_0 FROM revoked_licenses t2 WHERE cast(t1.ID as character varying) = t2.NPI_0))")
    end

    # physicians without invalid npis
    #SELECT count(*) FROM physicians t1 WHERE NOT EXISTS (SELECT t2.NPI_0 FROM revoked_licenses t2 WHERE cast(t1.ID as character varying) = t2.NPI_0)

    def set_validated_physicians
      psql("UPDATE physicians SET valid_npi = true WHERE id IN (SELECT t1.id FROM physicians t1 WHERE NOT EXISTS (SELECT t2.NPI_0 FROM revoked_licenses t2 WHERE cast(t1.ID as character varying) = t2.NPI_0))")
    end

  def write_validated_physicians_csv
    table = Physician.where(valid_npi: true)
    CSV.open( "#{Rails.root}/tmp/data/valid_carefirst_bluechoice_doctors", 'w' ) do |writer|
      writer << table.first.attributes.map { |a,v| a }
      table.each do |s|
        writer << s.attributes.map { |a,v| v }
      end
    end
  end

    def temp_table_columns_with_types
      @temp_table_columns_with_types ||= clean_file_headers.map do |column|
        column.gsub!('"', '')
        "#{column} VARCHAR(255)"
      end.join(', ')
    end

    def temp_table_columns
      @temp_table_columns ||= clean_file_headers.join(', ')
    end

    def clean_file_headers
      i = -1
      @clean_file_headers ||= File.open(load_input_file, &:readline).split(',').map do |column_name|
        i += 1
        "#{column_name.gsub(' ', '_').downcase.gsub(/^\"/, '').gsub(/\"$/, '').gsub(/[^A-Za-z]/, '_').strip}_#{i}"
      end
    end
end
