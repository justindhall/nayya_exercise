class Physician < ApplicationRecord
  def self.find_a_doctor(params)
    # physicians/new?action=find_a_doctor
    # —> request body: {location: xy, optional_params: [specialty, name, etc]}
    "list of doctors where validated and filtered by optional params"
    #ie sql = <<-SQL
      # SELECT *
  #       FROM physicians
  #         WHERE valid_npi = true
    #         AND WHERE address -> zip_code [location-2..location+2] <-- would likely want to replace approximating by zipcode with a more accurate geospatial approach given more time
    # SQL
    # pass in optional params as needed --> AND WHERE #{key = %#{param[value]}%}
  end
end
