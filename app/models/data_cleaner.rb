class DataCleaner
  def initialize(file_location='carefirst_bluechoice_doctors.txt')
    @file_location = "#{Rails.root}/tmp/data/#{file_location}"
  end

  def file_location
    @file_location
  end

  def parse
    json_file = File.open("#{Rails.root}/tmp/data/clean_carefirst_bluechoice_doctors.json", "w")
    Parallel.each(separate_data) do |string|
      cleaned = cleanData(string)
      json = JSON.parse(cleaned)
      json["data_type"] = json.delete("type")
      json_file.puts "#{json},"
      Physician.create(json)
    end
    json_file.close
  end

  def file_as_string
    @file_as_string ||= File.open(file_location, 'r').lines.first
  end

  def separate_data
    file_as_string.gsub(/\[\[|\]\]/, '').split(/(?<=]}]}),|(?<=]}]}]),/)
  end

  def replace_single_quotes_with_double(string)
    string.gsub(/(?<=[\s{\[])'|'(?=[,:\]])/, '"')
  end

  def replace_none_with_null(string)
    string.gsub('None', 'null')
  end

  def remove_stray_brackets(string)
    string.gsub(/\A\], \[|\A\[|\]\z|\A\s\[/, '')
  end

  def cleaner_addresses(string)
    string.gsub(/(?<='addresses': )\[|(?<=})\](?=})/, '')
  end

  def cleanData(string)
    with_null = replace_none_with_null(string)
    no_stray_brackets = remove_stray_brackets(with_null)
    address_as_object = cleaner_addresses(no_stray_brackets)
    replace_single_quotes_with_double(address_as_object)
  end
end
